<?php
/**
 * Created by PhpStorm.
 * User: victor
 * Date: 11.11.20
 * Time: 21:40
 */
$dsn = 'mysql:dbname='.($_ENV['DB_DATABASE'] ?? '').';host=db';
$user = $_ENV['DB_USERNAME'] ?? '';
$password = $_ENV['DB_PASSWORD'] ?? '';

try {
    $dbh = new PDO($dsn, $user, $password);
    echo 'Подключение удалось:';
} catch (PDOException $e) {
    echo 'Подключение не удалось: ' . $e->getMessage();
}